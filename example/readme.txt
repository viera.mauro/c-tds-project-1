c-tds-project-1 example using Co-operative - Time Driven System - Project for LPC1769

Example description
Required_SystemCoreClock => 100MHz
SysTick_RateHz => 1000 ticks per second (1mS)

Only one task:
task-test_gpio_lpc1769
    Simple switch interface code, without/with software debounce.
    Simple led interface code.
	

Special connection requirements
There are no special connection requirements for this example.

Build procedures:
Visit the LPCOpen quickstart guide at "http://www.lpcware.com/content/project/lpcopen-platform-nxp-lpc-microcontrollers/lpcopen-v200-quickstart-guides"
to get started building LPCOpen projects.
