/*--------------------------------------------------------------------*-

    task-test_gpio_lpc1769.c (Released 2019-03)

  --------------------------------------------------------------------

    Simple TEST_GPIO task for LPC1769.
	[Read Input & Write Output on LPCXpresso baseboard.]

    Simple switch interface code, without/with software debounce.
    Simple led interface code.

-*--------------------------------------------------------------------*/


// Project header
#include "../main/main.h"


// Task header
#include "task-test_gpio_lpc1769.h"


// ------ Public variable ------------------------------------------
uint32_t SW_switch_pressed_G = 0;


// ------ Private constants ----------------------------------------
#define TEST_1 (1)	/* Test original task */
#define TEST_2 (2)	/* Test MEF switch w/debounce */
#define TEST_3 (3)	/* Test MEF blinking led & switch w/debounce */
#define TEST_4 (4)	/* Test MEF led w/dimmer & switch w/debounce */
#define TEST_5 (5)	/* Test modified task # ... */
#define TEST_6 (6)	/* Test modified task # ... */

#define NO_TITILA 0
#define TITILA 1

#define TEST (TEST_4)


// ------ Private variable -----------------------------------------
//static uint8_t switch_input = 0;


/*------------------------------------------------------------------*-

    TEST_GPIO_Init()

    Prepare for TEST_GPIO_Update() function - see below.

-*------------------------------------------------------------------*/
void TEST_GPIO_Init(void)
{
	// Set up "TEST_GPIO" SW as an input pin
	Chip_IOCON_PinMux(LPC_IOCON, TEST_GPIO_SWITCH_PORT, TEST_GPIO_SWITCH_PIN, TEST_GPIO_SWITCH_PIN_MODE, TEST_GPIO_SWITCH_PIN_FUNC);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, TEST_GPIO_SWITCH_PORT, TEST_GPIO_SWITCH_PIN);

	// Set up "TEST_GPIO" LED as an output pin
	Chip_IOCON_PinMux(LPC_IOCON, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, TEST_GPIO_LED_PIN_MODE, TEST_GPIO_LED_PIN_FUNC);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN);

	// Switch not pressed
	SW_switch_pressed_G = SW_NOT_PRESSED;
}


#if (TEST == TEST_1)	/* Test original task */
/*------------------------------------------------------------------*-

    TEST_GPIO_Update()

    Simple switch interface code, without/with software debounce.
    Simple led interface code.

    If TEST_GPIO_SWITCH is not pressed,	TEST_GPIO_LED => LED_OFF
    If TEST_GPIO_SWITCH is     pressed, TEST_GPIO_LED => LED_ON

    Must schedule every 1 mili Second (soft deadline).

-*------------------------------------------------------------------*/
void TEST_GPIO_Update(void)
{
	// Read TEST_GPIO_SWITCH
	switch_input = Chip_GPIO_ReadPortBit(LPC_GPIO, TEST_GPIO_SWITCH_PORT, TEST_GPIO_SWITCH_PIN);
	if (switch_input == SW_PRESSED)
	{
		// Write TEST_GPIO_LED => LED_ON
    	Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_ON);

		// Switch pressed
		SW_switch_pressed_G = SW_PRESSED;
	}
	else
	{
		// Write TEST_GPIO_LED => LED_OFF
		Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);

		// Switch not pressed
		SW_switch_pressed_G = SW_NOT_PRESSED;
	}
}
#endif

#if (TEST == TEST_2)
void TEST_GPIO_Update(void)
{
	static uint8_t rebotes=0,valor_anterior=0,valor_actual=0;
	// Read TEST_GPIO_SWITCH
	valor_actual = Chip_GPIO_ReadPortBit(LPC_GPIO, TEST_GPIO_SWITCH_PORT, TEST_GPIO_SWITCH_PIN);
	if(valor_actual==valor_anterior)
		{
		rebotes++;
		valor_anterior=valor_actual;
		}
	else
		{
		rebotes=0;
		valor_anterior=valor_actual;
		}
	if(rebotes==50)
{
		rebotes=0;
	if (valor_actual == SW_PRESSED)
		{
		// Write TEST_GPIO_LED => LED_ON
    	Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_ON);

		// Switch pressed
		SW_switch_pressed_G = SW_PRESSED;
		}
	else
		{
		// Write TEST_GPIO_LED => LED_OFF
		Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);

		// Switch not pressed
		SW_switch_pressed_G = SW_NOT_PRESSED;
		}
	}
}
#endif

#if (TEST == TEST_3)
void TEST_GPIO_Update(void)
{
	static uint8_t rebotes=0,valor_anterior=0,valor_actual=0,estado=NO_TITILA;
	static uint32_t tiempo_led=0;
	// Read TEST_GPIO_SWITCH
	valor_actual = Chip_GPIO_ReadPortBit(LPC_GPIO, TEST_GPIO_SWITCH_PORT, TEST_GPIO_SWITCH_PIN);
	if(valor_actual==valor_anterior)
		{
		rebotes++;
		valor_anterior=valor_actual;
		}
	else
		{
		rebotes=0;
		valor_anterior=valor_actual;
		}
	if(rebotes==50)
{
	rebotes=0;
	if (valor_actual == SW_PRESSED)
	{
		if(estado==NO_TITILA) estado=TITILA;
		if(estado==TITILA) estado=NO_TITILA;

	if(estado==NO_TITILA)
			{

			// Write TEST_GPIO_LED => LED_OFF
			Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);

			}
		if(estado==TITILA)
					{
					tiempo_led++;
					// Write TEST_GPIO_LED => LED_ON
					Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_ON);
					if(tiempo_led>700)
						Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);
					if(tiempo_led>=1000) tiempo_led=0;

					}
		// Switch pressed
		SW_switch_pressed_G = SW_PRESSED;
		}
	else
		{
		// Write TEST_GPIO_LED => LED_OFF
				SW_switch_pressed_G = SW_NOT_PRESSED;
		}
	}
}
#endif

#if (TEST == TEST_4)
void TEST_GPIO_Update(void)
{
	static uint8_t rebotes=0,valor_anterior=0,valor_actual=0,duty_cycle=0;
	static uint32_t tiempo_led=0;
	// Read TEST_GPIO_SWITCH
	valor_actual = Chip_GPIO_ReadPortBit(LPC_GPIO, TEST_GPIO_SWITCH_PORT, TEST_GPIO_SWITCH_PIN);
	if(valor_actual==valor_anterior)
		{
		rebotes++;
		valor_anterior=valor_actual;
		}
	else
		{
		rebotes=0;
		valor_anterior=valor_actual;
		}
	if(rebotes==50)
{
	rebotes=0;
	if (valor_actual == SW_PRESSED)
	{
		duty_cycle=duty_cycle+20;
		if(duty_cycle>=100) duty_cycle=0;

		if(duty_cycle)
			{
			tiempo_led++;
			// Write TEST_GPIO_LED => LED_ON

			Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_ON);
			if(tiempo_led> duty_cycle)
				Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);

			if(tiempo_led>=100) tiempo_led=0;
			}
	else
		Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);





					}
		// Switch pressed
		SW_switch_pressed_G = SW_PRESSED;
		}
	else
		{
		// Write TEST_GPIO_LED => LED_OFF
				SW_switch_pressed_G = SW_NOT_PRESSED;
		}
	}

#endif
/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
